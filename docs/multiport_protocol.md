# Multi-port Serial Communication Protocol and Server

The Stellaris Launchpad offers a very convenient way for commnication between the launchpad and the PC it connected to. The ICDI (In-circuit Debug Interface) chip provides a virtual serial connection beside its debug related functions. The ICDI chip is then connected to Stellaris microcontroller at UART0 using a two wire async serial lines. It turns out this virtual serial connection is quite fast. I have tried running a plain echo program on the Stellaris and set the baud rate to 921Kbps. There is absolutely no error in communication when I sending several MB of data through the virtual serial connection. However, serial connections are a little out dated and many programs do not support it well. In addition, a program that uses a serial connection pretty much hold it until it finishes. There is no easy way to multi-plex the connection. 

When I created the support package for Stellaris Launchpad, I found this quite annoying because simulink external mode is something I desire. But by the same time, I still want additional data channels that can be used for console message displaying or sensor data transmission. Under this condition, I have created a very simple protocol to multiplex the virtual serial connection and support  multiple channels of communication simutaneously.


## Multi-port Serial Communication Protocol

The protocol is quite straightforward. Data transmission in both direction is marked with special characters that denotes the which port the following data is sending to. Serial connection has a data frame size of 8 bit, which is able to represent 256 symbols from 0x00 to 0xFF. Byte 0xF0 to 0xFF are reserved as special symbols in this protocol and any bytes in payload data that fall in this range is escaped by 0xFF (ESC character). Byte 0xF0 to 0xF9 correponds 10 ports this protocol supports. A table of symbols are listed below.

|        Symbol      |     Meaning                          |
|:------------------:|--------------------------------------|
|     0x0-0xEF       |   Payload data 0x0-0xEF              |
|   0xFF 0xF0-0xFA   |  Payload data 0xF0-0xFA              |
|     0xF0-0xF9      |  Switch to port 0-9 for the following data transmission         |
|        0xFA        |   Syncronization Request/Response    |

For example, if A sends data "12 34 56 78"(hex) to port 0 of the other end and B is sends data "F9 F0 AA BB" to port 1, then A sends more data "99 00 88 00" to port 0. The actual data transmission on the serial line is the following assuming previous transmission is to a port other than port 0.

    BYTE  0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 ... 
          F0 12 34 56 78 F1 FF F9 FF F0 AA BB F0 99 00 88 00 ...

Byte number 0 is 0xF0, which switches the port so that the following data is regarded as being sent to port 0. Byte 1 to 4 are payload data to port 0 from A. Then B start sending data to port 1, so byte 5 (0xF1) is inserted to denote data after this byte is for port 1. Byte 6-7 is the escaped character for payload 0xF9 and byte 8-9 is 0xF0 escaped. Byte 10-11 is payload "AA BB". Then at byte 12, 0xF0 switched the data transmission to port 0 again. The following dat is just the payload sending to port 0.

It entirely possible that data is sending to multiple ports at the same time by different applications, e.g., the external mode communication can happen while the Stellaris is sending messages to console. Transimission of data is scheduled using a prioritized scheme. Simply put, lower port numbers have higher priority than higher port numbers. So if there is pending data to both port 0 and port 1, data to port 0 is sent first.


It is noticed that correctness of the data depends on from which byte the decode starts. If the Stellaris or the PC skipped a few bytes before decoding, the meaning of the data is totally changed. For example, if the same data stream as above is send, but the receiver starts decoding at byte 7, then byte 8-11 will be "F0 AA BB" sending to port 9, not as what is intended. This means proper syncronization is expected at the beginning of communication. At connection after reset of the Stellaris or start of the PC side server program, Stellaris and PC are in un-synced state. By this time, any data transmission to the other party will results in a "request for sync" response. One side, usually the Stellaris will initiate the syncronization by sending two 0xFA (sync character), and the other side will response with one "0xFA" to signify successful of syncronization. After this, both party agreed on from which byte the decode should starts and the communication can proceed without ambiguity. Currently, there is no mechanism to guarantee an out-of-sync condition to be fixed during data transmission, which may be caused by data error.


## The Multi-port Communication API for Stellaris

There are 6 basic API functions that is associated with multi-port functionality. The first two are open and close port.

    int uart_port_open( unsigned int port,
                        unsigned char* in_buf, unsigned int in_buf_size,
                        unsigned char* out_buf, unsigned int out_buf_size)

This function initialize a port that can be read from or write to. The *port* is the port number and is from 0 to 9. The *in_buf* and *out_buf* are buffer spaces allocated by the caller and the *in_buf_size* and *out_buf_size* is the size of the input and output buffer respectively. This way the caller have full control of the buffer allocation and size. If this function returns 1 it is successful; if it returns 0, it means this port is already opened.

    int uart_port_close(unsigned int port)

This function close a port and set it to an uninialized state so that it can be open again. The parameter *port* is the port number. Return value of 1 means success and 0 means failed.

Besides open and close port, there are function that write to or read from the opened port.

    int uart_port_read_one(unsigned int port, unsigned char *buf)
    int uart_port_write_one(unsigned int port, unsigned char *buf)

The *uart_port_read_one* read a byte from the opened port. Similarly, *uart_port_write_one* write a byte to the opened port. Both funciton uses *buf* as a pointer to data buffer. Both functions do not block the execution. If these function returns 1 it means the call is successful. If they return 0, it means either there is noting to read (for uart_port_read_one) or the output buffer is full so that no more bytes can be write. 

The other two functions provide status information about the input or output buffer.

    int uart_port_read_pending(unsigned int port)
    int uart_port_write_pending(unsigned int port)

The *uart_port_read_pending* returns number of bytes still in the input buffer for the specified *port*. The *uart_port_write_pending* returns number of bytes still in the output buffer. These two functions are useful when specified size of data have to be read from or write to the port without error.

## Usage of the Multi-port Communication Server

On the PC host side, the multi-port communication capability is exposed using TCP or UDP ports because multiple TCP or UDP ports can be accessed simutaneously with many off-the-shelf tools and no more specialized APIs are needed. To achieve this, a multi-port communication server is developed. 

The server works like a hub, it redirects data that comes from the virtual serial port and destined to different port to the correspinding TCP or UDP port depend on the settings. It also agregate data from the dicated TCP or UDP together, adding meta data that signify its destination port and properly escape the data according to the protocol and then send it to Stellaris via the virtual serial port. Basically, all the communication between Stellaris and PC goes through this server. Thus, it is required to start the server before doing anything involves communication, e.g. using external mode, sending data to Stellaris or checking the debug print message.

To start the server, either run this following command in the MATLAB command window

    realtime.internal.runStellarisLPCmd('runcommsvr')

or open up the utility directory (<package path>\utils, where <package path> is *C:\MATLAB\SupportPackages\R2013a\StellarisLP* by default) and run the *MPCOM.exe* application.

Setting of the communication server depends on a *setting.ini* file in the same directory. There are two types of sections in this setting file. The first type is the serial configuration. For example,

    [SERIAL]
    PORT=6
    BAUD=115200

indicates "COM6" is the virtual COM port and baudrate used is 115200bps. The COM port on your computer may be different, you can check it from Windows Device Manager or using the Stellaris Support Package setup script *stellaris_lp_setup*. For the baud rate, changing it requires a corresponding modification in code on the Stellaris side. The baud rate Stellaris uses is controlled by a macrodefinition *UART_BAUD_RATE* in <package path>/src/uart_driver_mp.c.

There is only one SERIAL section in the setting file. But there are multiple port definition section. Each port *i* is defined with a "PORTi" section. There are two different types of ports: TCP and UDP ports. TCP port 12200+*i* is mapped with multi-port port *i* if it is initialized. If port *i* is a TCP port, data send to TCP port 12200+*i* will be redirect to port *i* on the Stellaris side and data send to Stellaris port *i* can be read from TCP port 12200+*i* on the PC side as well. UDP communication is similar but different port are used for sending and receiving because UDP is not based on connection. If port *i* is a UDP port, data sends to UDP port 12300+*i* will be route to Stellaris port *i* and data sending to port *i* can be received from UDP port 12400+*i* on the PC side. A UDP port also differs from a TCP port in that it have to specify a data packet size in the setting. Here are some example port setting sections.

    [PORT0] 
    TYPE=TCP
    AUTORESET=1

This section defines that port 0 will be a TCP port (the TYPE key) and AUTORESET is enabled. The auto reset function reset the TCP connection when the serial connection is reinitialize or re-synced. This way program that waiting on TCP data will not hang after Stellaris is being reset. Thus, it is useful for application like external mode. By default, auto reset is disabled so that application such as console window do not have to be restarted every time the program is downloaded.

    [PORT3]
    TYPE=UDP
    SIZE=6

This section defines that port 3 is associated with UDP (as the TYPE key specified) port. The size of the UDP packet is 6 bytes. The SIZE key is a mandatory key which have to be specified if the TYPE is UDP.

The setting file that comes with the package has port 0 and 1 as TCP port. Port 0 is for external mode communication and port 1 is for the console debug print by default. Sections can be commented with "#" character just as other ini setting files.


In order to view the debug print message, a terminal program such as putty or realterm should be used. The terminal program should connect to localhost:12201, which is the TCP port that corresponds to port 1 in multi-port.

The source code for MPCOM.exe is located in <git root>/utils/tcp_com_router.

## Examples of using multi-port in PC side

There are two examples in <git root>/utils showing how to access to multi-port from PC side. One example uses TCP protocol and the other uses UDP protocol. Both example works with the *stellaris_lp_blink_rc* demo of the support package.

The first example is *LightRemoterDemo*. It is written using C# and have a GUI interface. An extra port need to be configured in setting.ini.

    [PORT2]
    TYPE=TCP

The second example is *LightRemoterDemoUDP*. It is written using C++. The xPC UDP library is used for sending data. An extra port need to be configured as the following in setting.ini.

    [PORT2]
    TYPE=UDP
    SIZE=4



## Special tweaks for External Mode

As a legacy, the Stellaris external mode is using a serial protocol. However, with introduction of multi-port, it requires a TCP connection between Simulink and the communication server. To solve this mismatch problem, an additional external mode is defined in the *sl_customization.m* file under package installation directory. This additional mode uses a mex function called "ext_serial_win32_mp_comm".

This mex function (both 32 and 64 bit version) is already placed at <package path>/utils. For the record, I will put the command line that generates the mex file here.

    mex rtw\ext_mode\common\ext_comm.c ...
        rtw\ext_mode\common\ext_convert.c ...
        rtw\ext_mode\serial\ext_serial_transport.c ...
        rtw\ext_mode\serial\ext_serial_pkt.c ...
        rtw\ext_mode\serial\rtiostream_serial_interface.c ...
        rtw\ext_mode\common\ext_util.c ...
        -Irtw\c\src -Irtw\c\src\rtiostream\utils ...
        -Irtw\c\src\ext_mode\common ...
        -Irtw\c\src\ext_mode\serial -Irtw\ext_mode\common ...
        -Irtw\ext_mode\common\include ...
        -lmwrtiostreamutils ...
        -DEXTMODE_TCPIP_TRANSPORT -DSL_EXT_DLL ...
        -output toolbox\rtw\rtw\ext_serial_win32_mp_comm

If you want to generate this mex file yourself, cd to *matlabroot* in MATLAB first.
Notice that the source are for generating serial external mode mex file, but *EXTMODE_TCPIP_TRANSPORT* is defined to force it to use TCP transport layer, which make it able to connect to a TCP port. The resulting mex function will be output to where the "-output" switch specifies. Generally, this location have to be on your path for Simulink to detect its existence.


