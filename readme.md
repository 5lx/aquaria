# Aquaria 

Aquaria is a project that enables Simulink Run-on-Target-Hardware to platforms other than those officially supported by Mathworks. The Run-on-Target-Hardware feature allow user to build Simulink diagram into binaries that runs on embedded systems without the need for MATLAB Coder or Embedded Coder toolboxes, which are costy. Actually, even in the 99 dollars educational version support Run-on-Target-Hardware. Currently, the only embedded system supported is

* TI Tiva C/Stellaris Launchpad [TI][]

[TI]:http://www.ti.com/ww/en/launchpad/launchpads-tivac.html

Other platform are to be added soon.

If you are only interest in using the TI Tiva C/Stellaris Launchpad Support package, please follow the installation [here][].

[here]:https://bitbucket.org/sippey/aquaria/wiki/Installation%20Guide


If you have any questions or suggestions please do no hesitate to [contact me][].

[contact me]:mailto:sippey@gmail.com



