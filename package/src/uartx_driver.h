/*
 * uart_driver.h
 *
 *  Created on: Sep 30, 2013
 *      Author: Sippey
 */

#ifndef UARTX_DRIVER_H_
#define UARTX_DRIVER_H_

void UART1IntHandler(void);
void uartx_init(unsigned long baud);
int uartx_read_one(unsigned char* buf);
int uartx_write_one(unsigned char* buf);
int uartx_read_pending();
int uartx_write_pending();

#define UARTX_BASE              UART1_BASE
#define UARTX_TX_BUF_SIZE 		128
#define UARTX_RX_BUF_SIZE 		128


#endif /* UARTX_DRIVER_H_ */
