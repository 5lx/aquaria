// ;
// ; New ExitIsrAndRun Utility  
// ;
// ; Author: sippey (sippey@gmail.com)
// ; Date:   2014/04/10
// ;

#ifndef __REENTRANCE_H__
#define __REENTRANCE_H__

// [Example usage]
// 
// Note that the interrupt being made reentrant should have the lowest 
// priority.
//        
//     void SysTickHandler()
//     {
//         ExitIsrAndRun(reentrant_SysTickHandler);
//     }
// 
//     void reentrant_SysTickHandler()
//     {
//         // process that can be re-entrant
//     }
// 
//     void SVCallHandler()
//     {
//         if (GET_SVC_NUMBER == SVC_CALL_NUMBER)
//             ExitIsrAndRun_SVC();
//         else{
//             //.....
//         }
//     }
//     
// The interrupt table should be set up as 
//
//     void (* const g_pfnVectors[])(void) =
//     {
//         (void (*)(void))((unsigned long)&__STACK_TOP),
//         // The initial stack pointer
//         ResetISR,                               // The reset handler
//         NmiSR,                                  // The NMI handler
//         .
//         .
//         .
//         SvcCallHandler,                         // SVCall handler        .
//         .
//         .
//         .
//         SysTickIntHandler,                      // The SysTick handler
//         .
//         .
//         .
//     };


#define LAZY_STACKING         0     // if lazy_stacking is enabled this have to be 1
#define SVC_CALL_NUMBER       0     // SVC_CALL_NUMBER being used
#define GEN_SVC_HANDLER       0     // 1: Generate a general svc handler
                                    // void SVC_Handler_Other() should be
                                    // defined to handle other svc call

void ExitIsrAndRun( void (*fcn)(void) );
void ExitIsrAndRun_SVC(void );

#endif //__REENTRANCE_H__
