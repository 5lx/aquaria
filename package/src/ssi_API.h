#ifndef	_SSI_API_H_
#define _SSI_API_H_

void ssi0_init(void);
//void ssi0_ss(unsigned long sel);
void ssi0_write(unsigned long data);
void ssi0_read(unsigned long * pdata);

int ssi0_ss2(unsigned long sel);

#endif// _SSI_API_H_
