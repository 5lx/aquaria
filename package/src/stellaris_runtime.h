#ifndef __STELLARIS_RUNTIME_H__
#define __STELLARIS_RUNTIME_H__

void system_init(void);
void system_reset(void);
void system_safe(void);
void system_delay_ms(int ms);

//ADC declaration
void 			adc_init(void);
void 			adc_trigger(void);
unsigned int 	adc_get_channel(int ch_id);
unsigned int    adc_get_channel_mV(int ch_id);

//PWM declaration
void 			pwm_init();
void 			pwm_shutdown();
void            pwm_init_channel(int ch_id, unsigned int freq, int invert, int control);
void            pwm_set_channel(int ch_id, unsigned int percentX);
void            pwm_set_channel_f(int ch_id, float percent);
void            pwm_set_pd6_clk();

//COUNTER declaration
void 			counter_init(void);
void 			counter_init_channel(int ch_id);

unsigned long long counter_read_channel_pr(int ch_id);
unsigned long 	counter_read_channel(int ch_id);
int 			counter_read_diff(int ch_id);

// GPIO declaration
void            gpio_init(void);

// console declaration
void            console_init(void);
void            console_putc(char c);

// i2c declarations
void i2c_init(void);
void i2c_reset(void);

int i2c_readbytes(unsigned char dev, unsigned char reg, unsigned int length, unsigned char* data);
int i2c_readbyte(unsigned char dev, unsigned char reg, unsigned char* data);
int i2c_readbits(unsigned char dev, unsigned char reg,  int start, int length, unsigned char *data);

int i2c_writebytes(unsigned char dev, unsigned char reg, unsigned int length, unsigned char* data);
int i2c_writebyte(unsigned char dev, unsigned char reg, unsigned char data);
int i2c_writebits(unsigned char dev, unsigned char reg,  int start, int length, unsigned char data);
int i2c_writebit(unsigned char dev, unsigned char reg, int start, int data) ;

// spi declaration
#include "ssi_API.h"


// debug declaration
void            led_debug_signal(int id);

#define LED_RED     0x2
#define LED_BLUE    0x4
#define LED_GREEN   0x8
#define LED_ALL     (LED_RED|LED_BLUE|LED_GREEN)

#define     LED_DEBUG_RESET     0
#define     LED_DEBUG_OVERRUN   1
#define     LED_DEBUG_ALL_ON    2
#define     LED_DEBUG_ALL_OFF   3
#define     LED_DEBUG_REENTRANCE 4


// real time tick setup
#define     TICK_DEBUG  0
void            tick_init(float step_size);
void            systick_init(float step_size);
void            systick_intr_enable(void);
void            systick_intr_disable(void);
void            check_overrun();

// GPIO definition
// The following are defined in hw_gpio.h
//#define GPIO_UNLOCK_KEY (0x4C4F434B)
//#define GPIO_O_LOCK (0x520)
//#define GPIO_O_COMMIT (0x524)

// PWM definition
#define PWM_CTRL_ENABLE 	1
#define PWM_CTRL_DISABLE 	2
#define PWM_PERCENT_MULTIPLIER 		(1<<10)

// I2C definition
#define I2C_DEFAULT     I2C0_MASTER_BASE

// console i/o definition
#define CONSOLE_PORT  1
#define CONSOLE_N_IN_BUF    256
#define CONSOLE_N_OUT_BUF   256

// extmode i/o definition
// #define EXTMODE_PORT  0 // defined in rtiostreamserial_mp.c

#endif // __STELLARIS_RUNTIME_H__
