function sl_customization( cm)
% the first four is just to make it compatible with original realtime
% toolbox
    cm.ExtModeTransports.add('realtime.tlc', 'tcpip', 'ext_comm', 'Level1') ;
    cm.ExtModeTransports.add('realtime.tlc', 'serial_byteswap', 'ext_serial_win32_comm_no_acks_rtt_byteswap', 'Level1') ;
    cm.ExtModeTransports.add('realtime.tlc', 'serial', 'ext_serial_win32_comm_no_acks_rtt', 'Level1') ;
    cm.ExtModeTransports.add('realtime.tlc', 'serial_arduino', 'ext_serial_win32_comm', 'Level1') ;
% this added one send serial like package via 
    cm.ExtModeTransports.add('realtime.tlc', 'serial_hybrid_mp', 'ext_serial_win32_mp_comm', 'Level1') ;
end
