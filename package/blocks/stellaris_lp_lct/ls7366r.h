/*
 * ls7366r.h
 *
 *  Created on: Nov 11, 2013
 *      Author: Sippey
 */

#ifndef LS7366R_H_
#define LS7366R_H_

#ifdef MATLAB_MEX_FILE

    #define init_ls7366r(s,clk)  ((void)(0))
    #define ls7366r_get_encoder(s)  (0)

#else

    void init_ls7366r(int ssel, int clk);
    unsigned int ls7366r_get_encoder(int ssel);

#endif

#endif /* LS7366R_H_ */
