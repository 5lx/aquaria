#ifdef MATLAB_MEX_FILE
// build mex file
    #include "datapacket.h"
    // no code in this one.
#else

#include <stdlib.h>   // malloc
#include "stellaris_runtime.h"
#include "uart_driver_mp.h"
#include "datapacket.h"

int init_datapacket(int pkt_size, int port)
{
    unsigned char* in_buf = (unsigned char*)malloc(pkt_size*2);
    unsigned char* out_buf = (unsigned char*)malloc(1);
    
    if (in_buf && out_buf && uart_port_open(port, in_buf, pkt_size*2, out_buf, 1) )
    {
        return 1;
    }else
    {
        return 0;
    }
}

int datapacket_get(int pkt_size, int port, unsigned char *data)
{
    int n;
    unsigned char* data2=data;
    //printf("a=%d\n", uart_port_read_pending(port));
    
    if (uart_port_read_pending(port)>=pkt_size)
    {
        data[0]=1;
        n= pkt_size;
        
        while(n--)
        {
            uart_port_read_one(port, data++); // data copy
        }
        return 1;
    }
    else
        return 0;
}


#if 0
int init_datapacket(int pkt_size, int port, p_datapacket_reg p)
{
    p->in_buf = (unsigned char*)malloc(pkt_size*2);
    p->out_buf = (unsigned char*)malloc(1);
    
    if (p->in_buf &&  p->out_buf && uart_port_open(port, p->inbuf, pkt_size*2, p->out_buf, 1) )
    {
        p->port = port;
        p->pkt_size = pkt_size;
        input_synced = SYNC_THRESHOLD+1;
        return 1;
    }else
    {
        destroy_datapacket(p);
        return 0;
    }
}

void destroy_datapacket(p_datapacket_reg p)
{
    uart_port_close(p->port);
    
    if (p->in_buf)
        free(p->in_buf);
    if (p->out_buf)
        free(p->out_buf);

    p->port = UART_N_PORTS; // an invalid port , valid port [0..UART_N_PORTS-1]
    p->in_buf = p->out_buf = NULL;
}

int attemp_sync_input(p_datapacket_reg p)
{
    unsigned char ch;
    
    while (uart_port_read_one(p->port, &ch))
    {
        if (ch==SYNC_CHAR  )
        {
            if (p->input_synced<SYNC_THRESHOLD)
                p->input_synced++;
        }else if (ch==((~SYNC_CHAR)&0xFF) && p->input_synced==SYNC_THRESHOLD){
            p->input_synced ++;
        }else
        {
            p->input_synced=0;
        }
    }
    return 0;
}

void datapacket_get(p_datapacket_reg p, unsigned char * data)
{
    int n;
    if (p->input_synced<=SYNC_THRESHOLD)
    {
        if (!attemp_sync_input(p))
            return;
    }
    if (uart_port_read_pending(p->port)>=pkt_size)
    {
        n= pkt_size;
        while(n--)
        {
            uart_port_read_one(p->port,*data++); // data copy
        }
    }
}

void datapacket_put(p_datapacket_reg p, unsigned char * data)
{
    //do nothing
}
#endif //0


#endif
