function def = lctdef_LS7366R()
%% LS7366R
LS7366R = legacy_code('initialize');
LS7366R.SFunctionName = 'stellaris_lp_sfunc_LS7366R';
LS7366R.HeaderFiles = {'ls7366r.h'};
LS7366R.SourceFiles = {'ls7366r.c'};
LS7366R.StartFcnSpec = 'init_ls7366r(int32 p1, int32 p2)';
LS7366R.OutputFcnSpec = 'int32 y1 = ls7366r_get_encoder(int32 p1)';
LS7366R.SampleTime = 'parameterized';

def = LS7366R(:);
end