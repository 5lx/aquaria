function def = lctdef_xdatapackin()
%% DataPackin
XDataPackin = legacy_code('initialize');
XDataPackin.SFunctionName = 'stellaris_lp_sfunc_XDataPackin';
XDataPackin.HeaderFiles = {'xdatapacket.h'};
XDataPackin.SourceFiles = {'xdatapacket.c'};
XDataPackin.StartFcnSpec = 'init_xdatapacket(int32 p1, int32 p2)'; 
XDataPackin.OutputFcnSpec = 'int32 y2 = xdatapacket_get(int32 p1, uint8 y1[p1])';
XDataPackin.SampleTime = 'parameterized';

def = XDataPackin(:);
end