function def = lctdef_datapackin()
%% DataPackin
DataPackin = legacy_code('initialize');
DataPackin.SFunctionName = 'stellaris_lp_sfunc_DataPackin';
DataPackin.HeaderFiles = {'datapacket.h'};
DataPackin.SourceFiles = {'datapacket.c'};
DataPackin.StartFcnSpec = 'init_datapacket(int32 p1, int32 p2)';
DataPackin.OutputFcnSpec = 'int32 y2 = datapacket_get(int32 p1, int32 p2, uint8 y1[p1])';
DataPackin.SampleTime = 'parameterized';

def = DataPackin(:);
end