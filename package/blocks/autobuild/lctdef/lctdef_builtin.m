function def = lctdef_builtin()
%% GPIO Write
% Populate legacy_code structure with information
GPIOWrite = legacy_code('initialize');
GPIOWrite.SFunctionName = 'stellaris_lp_sfunc_GPIOWrite';
GPIOWrite.HeaderFiles = {'gpiolct.h'};
GPIOWrite.SourceFiles = {'gpiolct.c'};
GPIOWrite.OutputFcnSpec = 'GPIOWrite(uint32 p1, uint8 u1, uint8 u2)';
GPIOWrite.SampleTime = 'parameterized';
% Support calling from within For-Each subsystem
GPIOWrite.Options.supportsMultipleExecInstances = true;
%% GPIO Read
GPIORead = legacy_code('initialize');
GPIORead.SFunctionName = 'stellaris_lp_sfunc_GPIORead';
GPIORead.HeaderFiles = {'gpiolct.h'};
GPIORead.SourceFiles = {'gpiolct.c'};
GPIORead.OutputFcnSpec = 'int32 y1 = GPIORead(uint32 p1, uint8 u1)';
GPIORead.SampleTime = 'parameterized';
GPIORead.Options.supportsMultipleExecInstances = true;
%% GPIO Setup
GPIOSetup = legacy_code('initialize');
GPIOSetup.SFunctionName = 'stellaris_lp_sfunc_GPIOSetup';
GPIOSetup.HeaderFiles = {'gpiolct.h'};
GPIOSetup.SourceFiles = {'gpiolct.c'};
GPIOSetup.StartFcnSpec = 'GPIOSetup(uint32 p1, uint32 p2, uint8 p3, uint8 p4)';
% avoid being pure
GPIOSetup.OutputFcnSpec = 'GPIOVoidFunc()';
GPIOSetup.Options.supportsMultipleExecInstances = true;
%% Push Buttons
Buttons = legacy_code('initialize');
Buttons.SFunctionName = 'stellaris_lp_sfunc_Buttons';
Buttons.HeaderFiles = {'buttonslct.h'};
Buttons.SourceFiles = {'buttonslct.c'};
Buttons.StartFcnSpec = 'ButtonsInit()';
Buttons.SampleTime = 'parameterized';
Buttons.OutputFcnSpec = 'uint8 y1 = ButtonsRead(uint8 y2[1])';
%% UART Read
UARTRead = legacy_code('initialize');
UARTRead.SFunctionName = 'stellaris_lp_sfunc_UARTRead';
UARTRead.HeaderFiles = {'uartlct.h'};
UARTRead.SourceFiles = {'uartlct.c'};
UARTRead.StartFcnSpec = 'UARTInit()';
UARTRead.OutputFcnSpec = 'int32 y1 = UARTRead(uint8 p1, uint8 y2[1])';
UARTRead.Options.supportsMultipleExecInstances = true;
%% UART Write
UARTWrite = legacy_code('initialize');
UARTWrite.SFunctionName = 'stellaris_lp_sfunc_UARTWrite';
UARTWrite.HeaderFiles = {'uartlct.h'};
UARTWrite.SourceFiles = {'uartlct.c'};
UARTWrite.StartFcnSpec = 'UARTInit()';
UARTWrite.OutputFcnSpec = 'UARTWrite(uint8 p1, uint8 u1)';
UARTWrite.Options.supportsMultipleExecInstances = true;
%% Built-in temperature sensor
TempSensor = legacy_code('initialize');
TempSensor.SFunctionName = 'stellaris_lp_sfunc_TempSensor';
TempSensor.HeaderFiles = {'tempsensorlct.h'};
TempSensor.SourceFiles = {'tempsensorlct.c'};
TempSensor.StartFcnSpec = 'TempSensorInit()';
TempSensor.OutputFcnSpec = 'uint32 y1 = TempSensorRead(uint8 p1)';

%% ADC
ADCRead = legacy_code('initialize');
ADCRead.SFunctionName = 'stellaris_lp_sfunc_ADCRead';
ADCRead.HeaderFiles = {'lct_wrapper.h'};
%ADCRead.SourceFiles = {'adclct.c'};
%ADCRead.StartFcnSpec = 'ADCInit()';
ADCRead.OutputFcnSpec = 'uint32 y1 = ADCRead(int32 p1)';
ADCRead.SampleTime = 'parameterized';

%% PWM
PWMUpdate = legacy_code('initialize');
PWMUpdate.SFunctionName = 'stellaris_lp_sfunc_PWMUpdate';
PWMUpdate.HeaderFiles = {'lct_wrapper.h'};
%PWMUpdate.SourceFiles = {'pwmlct.c'};
PWMUpdate.StartFcnSpec = 'PWMInit(int32 p1, uint32 p2, int32 p3)'; % ch, freq, inverted
PWMUpdate.OutputFcnSpec = 'PWMUpdate(int32 p1, uint32 u1)'; % ch, pct
PWMUpdate.TerminateFcnSpec = 'PWMStop(int32 p1)'; % ch

%% PWM f
PWMUpdate_f = legacy_code('initialize');
PWMUpdate_f.SFunctionName = 'stellaris_lp_sfunc_PWMUpdate_f';
PWMUpdate_f.HeaderFiles = {'lct_wrapper.h'};
%PWMUpdate_f.SourceFiles = {'pwmlct.c'};
PWMUpdate_f.StartFcnSpec = 'PWMInit(int32 p1, uint32 p2, int32 p3)';  % ch, freq, inverted
PWMUpdate_f.OutputFcnSpec = 'PWMUpdate_f(int32 p1, single u1)'; % ch, pct
PWMUpdate_f.TerminateFcnSpec = 'PWMStop(int32 p1)'; % ch

%% Serial Debug Printf
DebugPrint = legacy_code('initialize');
DebugPrint.SFunctionName = 'stellaris_lp_sfunc_DebugPrint';
DebugPrint.HeaderFiles = {'lct_wrapper.h'};
%DebugPrint.SourceFiles = {'dbgprintlct.c'};
%DebugPrint.StartFcnSpec = 'DebugPrintInit()';
DebugPrint.OutputFcnSpec = 'DebugPrint(uint8 p1[], int32 u1, int32 u2, int32 u3, int32 u4)';
DebugPrint.SampleTime = 'parameterized';

%% Serial Debug Printf V2
DebugPrint2 = legacy_code('initialize');
DebugPrint2.SFunctionName = 'stellaris_lp_sfunc_DebugPrint2';
DebugPrint2.HeaderFiles = {'lct_wrapper.h'};
%DebugPrint.SourceFiles = {'dbgprintlct.c'};
%DebugPrint.StartFcnSpec = 'DebugPrintInit()';
DebugPrint2.OutputFcnSpec = 'DebugPrint2(uint8 p1[], uint8 u1[])';
DebugPrint2.SampleTime = 'parameterized';


%% Put multiple registration files together
def = [GPIOWrite(:); GPIORead(:);GPIOSetup(:);Buttons(:);
       %UARTRead(:);UARTWrite(:);TempSensor(:);
       ADCRead(:);PWMUpdate(:);PWMUpdate_f(:); DebugPrint(:); DebugPrint2(:)];

end