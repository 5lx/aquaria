function ret = stellaris_lp_build_init(varargin)
%rtw.targetNeedsCodeGen( 'set', true) ;
h = varargin{1};
set_param( h.ModelName, 'ObfuscateCode', '0' ); % force it not to ObfuscateCode
%set_param( h.ModelName, 'ObfuscateCode', '3' ); % force it to ObfuscateCode
%set_param( h.ModelName, 'PackageGeneratedCodeAndArtifacts','off'); % save the code for analysis

hPkgInfo = realtime.internal.getStellarisLPInfo('PkgInfo');
spPkg= hPkgInfo.getSpPkgInfo( 'Stellaris LP') ;
set_mkt_toolchain(spPkg.RootDir);

ret = true;
end

function set_mkt_toolchain(pkg_root_dir)
%%
xmake_path = fileparts(which('xmakefilesetup'));
if isempty(xmake_path) || ~exist(fullfile( xmake_path, 'registry', 'templates' , 'defgmake.mkt'), 'file')
    % use package defgmake
    linkfoundation.xmakefile.XMakefilePreferences.setUserConfigurationLocation( fullfile( pkg_root_dir, 'registry') ) ;
    linkfoundation.xmakefile.XMakefilePreferences.setActiveTemplate( 'gmake') ;
else
    %use stellaris_lp_gmake
    linkfoundation.xmakefile.XMakefilePreferences.setActiveTemplate( 'stellaris_lp_gmake') ;
end


linkfoundation.xmakefile.XMakefilePreferences.setUserConfigurationLocation( fullfile( pkg_root_dir, 'registry') ) ;
linkfoundation.xmakefile.XMakefilePreferences.setActiveConfiguration( 'stellaris_lp') ;  % name without space

linkfoundation.xmakefile.XMakefileConfiguration.getConfigurations( true) ;
linkfoundation.xmakefile.XMakefileTemplate.reload( ) ;

end
