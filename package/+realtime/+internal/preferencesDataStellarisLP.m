function info = preferencesDataStellarisLP(~)

tgtData= get_param( bdroot, 'TargetExtensionData') ;

info.COMPort= [ 'COM', tgtData.COM_port_number] ;
info.UploadRate= tgtData.COM_port_baud_rate;

if isequal( tgtData.( 'Set_host_COM_port') , 0)
%{ 
    % Automatically detect COM port. 
    try
        portInfo= realtime.internal.getArduinoBoardCOMPort( 'Uno') ;
    catch ME%#ok<NASGU>
        return ;
    end
    info.COMPort= portInfo.COMPort;
    info.UploadRate= portInfo.UploadRate;
%}
    % Dumb implementation
    info = realtime.internal.getStellarisLPInfo('COMPORT');
end

info.InstallDir= ''; % dir of compiler ??? in arduino it is P:\Libs\Matlab\Targets\arduino-1.0

end
