function info= toolsInfoDataStellarisLP( ~ )


info.IncludePathDelimiter= '-I';
info.PreprocSymbolDelimiter= '--define=';
info.RTIOStreamFileName= 'rtiostreamserial.c';
info.PreBuildUtility= '@realtime.internal.stellarislpPreBuild';
info.BuildUtility= '';
info.PreDownloadUtility= '@realtime.internal.stellarislpPreDownload';
info.PreDownloadUtilityFlags= '';
info.PreDownloadFileExtension= '';
info.DownloadUtility= ''; %path to download utility
info.DownloadUtilityFlags= ''; %???
info.DownloadFileExtension= '.out';
info.PostDownloadUtility= '';
info.PostDownloadUtilityFlags= '';
info.PostDownloadFileExtension= '';
info.RunUtility= '';
info.RunUtilityFlags= '';
info.RunFileExtension= '';
info.PostRunUtility= '';
info.RunOnEntry= 'realtime.internal.stellaris_lp_build_init';
info.ErrorHandling= '';
info.XMakefileTemplate = 'gmake';
info.XMakefileConfiguration = 'stellaris_lp';
info.XMakefileTemplateLocation = fullfile(  ...
    realtime.internal.getStellarisLPInfo('PackageDir'), 'registry' );
end