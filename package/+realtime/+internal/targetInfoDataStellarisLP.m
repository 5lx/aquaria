function info= targetInfoDataStellarisLP(~)

%cominfo = realtime.internal.getStellarisLPInfo('COMPORT');

info. ProdHWDeviceType= 'ARM Compatible->ARM Cortex';

info. ExtModeTrigDuration= 2;
info. ExtModeConnectPause= 1;

info. ExtModeTransport= 4;
info. ExtModeMexArgsInit= '''127.0.0.1''  1  12200';

%info. ExtModeTransport= 3;
%info. ExtModeMexArgsInit= ['0 ' cominfo.COMPortNumber ' ' '256000'];%cominfo.UploadRate]; 

info. RTTParams= i_getRTTParams(info.ExtModeTransport);
end


function rttparams= i_getRTTParams(mode)
rttparams=  ...
    {
    'ExtModeTransport', mode, mode;
%    'SolverMode','MultiTasking','MultiTasking'
    'SolverMode','SingleTasking',{'SingleTasking', 'MultiTasking'}
    } ;
end