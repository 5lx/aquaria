function ret = runStellarisLPCmd (cmd, varargin )
switch (cmd)
    case 'download'
        narginchk(3, 3);
        ret = i_stellaris_lp_download(varargin{1}, varargin{2});
    case 'setup'
        narginchk(1, 1);
        i_stellaris_lp_setup
        ret =1;
    case 'opendemo'
        narginchk(2, 2);
        open_system(['stellaris_lp_' varargin{1} ]);
        ret =[]; 
    case 'compilecomm'
        i_stellaris_lp_compilecomm
    case 'buildblock'
        if nargin == 2
            i_stellaris_lp_buildblock(varargin{1})
        end
    case 'blockdecltmpl'
        
        if exist(fullfile('.','rtwmakecfgStellarisLP_user.m'),'file')
            reply = input('File exist in current directory. Do you want overwrite? Y/N [N]:','s');
            if isempty(reply)
              reply = 'N';
            end
            if reply == 'N'
                return
            end
        end
        copyfile(fullfile(realtime.internal.getStellarisLPInfo('PackageDir'),...
                          'registry','rtwmakecfgStellarisLP_user.m.tmpl'), ...
                 'rtwmakecfgStellarisLP_user.m');
    case 'runcommsvr'
        d = pwd();
        cd(fullfile(realtime.internal.getStellarisLPInfo('PackageDir'), ...
             'utils'));
        system('MPCOM.exe  &');
        cd(d);
    case 'editcommsvrcfg'
        edit(fullfile(realtime.internal.getStellarisLPInfo('PackageDir'), ...
             'utils','setting.ini'));
    otherwise
        error('Invalid Stellaris LP command!');
end
end

function i_stellaris_lp_buildblock(def)
%def = lctdef_LS7366();
legacy_code('SFCN_CMEX_GENERATE', def);
legacy_code('COMPILE', def);
delete([def.SFunctionName '.cpp']);

%legacy_code('SLBLOCK_GENERATE', def, ['block_' def.SFunctionName]);

legacy_code('rtwmakecfg_generate', def);
C = textread('rtwmakecfg.m', '%s','delimiter', '\n'); %#ok<REMFF1>
l= strmatch('% Dependency info for S-function ', C); %#ok<MATCH2>
delete('rtwmakecfg.m');


disp(['Clip the following between double lines and paste into ' , ...
      'rtwmakecfgStellarisLP_user in your path']);
disp('==================================================================');
fprintf(['%%%% ' def.SFunctionName '\ninfo = info_tmpl;' '%s' '\narray = [array info];\n'],strjoin(unique(C(l+1:end)'),'\n'));
disp('==================================================================');
end

function i_stellaris_lp_compilecomm()
d = pwd();
try
cd(matlabroot())

% serial protocol on a tcp transport
mex rtw\ext_mode\common\ext_comm.c ...
    rtw\ext_mode\common\ext_convert.c ...
    rtw\ext_mode\serial\ext_serial_transport.c ...
    rtw\ext_mode\serial\ext_serial_pkt.c ...
    rtw\ext_mode\serial\rtiostream_serial_interface.c ...
    rtw\ext_mode\common\ext_util.c ...
    -Irtw\c\src -Irtw\c\src\rtiostream\utils ...
    -Irtw\c\src\ext_mode\common ...
    -Irtw\c\src\ext_mode\serial -Irtw\ext_mode\common ...
    -Irtw\ext_mode\common\include ...
    -lmwrtiostreamutils ...
    -DEXTMODE_TCPIP_TRANSPORT -DSL_EXT_DLL ...
    -output toolbox\rtw\rtw\ext_serial_win32_mp_comm
catch err
    cd(d);
    rethrow(err);
end
cd(d)

end

% This function is modified from Mikhail's Stellaris package
function ret = i_stellaris_lp_download(modelName, output_path)

disp(['### Downloading ', modelName, ' to Stellaris LaunchPad...']);

TargetRoot = realtime.internal.getStellarisLPInfo('PackageDir');
CCSRoot = realtime.internal.getStellarisLPInfo('CCSDir');
%ArmCC = realtime.internal.getStellarisLPInfo('ArmCCDir');
%{
if (ischar(makertwObj)) %String 'PIL'
    outfile = modelName;
else
    outfile = fullfile(makertwObj.BuildDirectory, [modelName, '.out']);
    %outfilebin = fullfile(makertwObj.BuildDirectory, [modelName, '.bin']);
end
%}

% % obj2bin tools 
% ofd = fullfile(ArmCC,'bin','armofd');
% hex = fullfile(ArmCC,'bin','armhex');
% mkhex = fullfile(TargetRoot,'utils','tiobj2bin','mkhex4bin');
% tiobj2bin = fullfile(TargetRoot,'utils','tiobj2bin','tiobj2bin');
% flash util
lm4flash =  fullfile(TargetRoot,'utils','lm4flash','lmflash');

outfile = fullfile(output_path, [modelName, '.out']);
binfile = fullfile(output_path, [modelName, '.bin']);

if isunix
    % Convert out to bin for lm4flash
    %system([CCSRoot,'/utils/tiobj2bin/tiobj2bin ',outfile,' ',outfilebin,' ',...
    %    CompilerRoot,'/bin/armofd ',CompilerRoot, '/bin/armhex ',CCSRoot,'/utils/tiobj2bin/mkhex4bin']);
    %TODO fail gracefully here.
    %system(['lm4flash ',outfilebin]);
    ret = system(['LD_PRELOAD="',CCSRoot,'/ccs_base/DebugServer/bin/libti_xpcom.so" "',CCSRoot,'/ccs_base/scripting/examples/loadti/loadti.sh" -a -r ',...
        '-c "',TargetRoot,'/registry/Stellaris_LaunchPad.ccxml" ',...
        '"',outfile,'" 2> /dev/null']);
else
    %construct hex file taken care of in stellaris_lp.m in registry
    %ret = system(['"' tiobj2bin '" "' outfile '" "' binfile  '" "' ofd '" "' hex '" "' mkhex '"']);
    ret = system(['"' lm4flash '" -q ek-lm4f120xl -r "' binfile '" >NUL']);
    
    % old loadti.bat loading mechanism
    %ret = system(['"',CCSRoot,'/ccs_base/scripting/examples/loadti/loadti.bat" -a -r ',...
    %    '-c "',TargetRoot,'/registry/Stellaris_LaunchPad.ccxml" ',...
    %    '"',outfile,'" 2 > NUL']);
end

if ret == 0
    disp(['### Successfully downloaded ', modelName, ' to Stellaris LaunchPad.']);
    ret =1;
else
    disp(['### Failed to download ', modelName, ' to Stellaris LaunchPad.']);
    ret =0;
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function i_stellaris_lp_setup
    pref_group = 'stellaris_lp';
    
    setpref(pref_group,'COMPort',setup_com_port);
    [CCSDir, ~, StellarisWareDir] = ccs_setup_paths;
    setpref(pref_group,'CCSDir',CCSDir);
    %addpref(pref_group,'CompilerRoot',CompilerRoot);
    setpref(pref_group,'StellarisWareDir',StellarisWareDir);
end

%
% code below is from Mikhail's Stellaris package.
% 
%

function [CCSRoot, CompilerRoot, StellarisWareRoot] = ccs_setup_paths()
	% TODO: make it foolproof
    CCSRoot = fix_slash(uigetdir(matlabroot,'CCS root directory: (the one with ccs_base, tools ...)'));
    CompilerRoot = '';%fix_slash(uigetdir(CCSRoot,'CCS Compiler root directory: (tools/compiler/arm_5.X.X)'));
    StellarisWareRoot = fix_slash(uigetdir(CCSRoot,'StellarisWare root directory: (the one with boards, driverlib ...)'));
end

function COMPort = setup_com_port()

[ports, names] = find_com_ports;

% Choose COM port
[selection,ok] = listdlg('PromptString','Choose TI Stellaris LaunchPad COM port:',...
    'SelectionMode','single',...
    'ListSize',[260 160],...
    'ListString',names);
if (ok == 1 && selection > 2) %have actually chosen COM port
    COMPort = char(ports{selection-2}); % -2 for padding with names array
elseif (ok == 1 && selection > 1) %chosen to refresh COM Ports
    COMPort = setup_com_port();
else %chosen to enter manually or canceled
    COMPort = cell2mat(inputdlg('Enter COM port manually: (ex. COM3 or ttyACM0)','COM port',1));
end
end

function [ports, names] = find_com_ports()
%Find COM ports
names_string = {'Enter COM port manually...','Refresh COM ports list...'};
if isunix
    %TODO
	%check /dev/serial
	unixCmd = 'ls -l /dev/serial/by-id/*';
	[unixCmdStatus,unixCmdOutput]=system(unixCmd);
	if (unixCmdStatus > 0)
		ports = {};
		names = {};
	else
		%names = regexp(unixCmdOutput,'(?<=/dev/serial/by-id/)\S+','match');
		%ports = regexp(unixCmdOutput,'(?<=->.*/)tty\w+','match');
		[names, ports] = regexp(unixCmdOutput,'(?<=/dev/serial/by-id/)\S.*?((?<=->.*/)tty\w+)','match','tokens');
	end
else
    wmiCmd = ['wmic /namespace:\\root\cimv2 '...
              'path Win32_SerialPort get DeviceID,Name'];
    %TODO catch error (wmic is not on WinXP Home for instance).
    [~,wmiCmdOutput]=system(wmiCmd);
    % in a single regexp call with tokens
    [names, ports] = regexp(wmiCmdOutput,'(?<=COM\d+\s*)\S.*?\((COM\d+)\)','match','tokens');
    % same in two regexp calls..
    %ports = regexp(wmiCmdOutput,'COM\d+(?!\))','match');
    %names = regexp(wmiCmdOutput,'(?<=COM\d+\s*)\S.*?\(COM\d+\)','match');
    %regCmd=['reg query '...
    %        'HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM'];
    %[~,regCmdOutput]=system(regCmd);
    %ports = regexp(regCmdOutput,'COM\d+','match');
end
names = [names_string,names];
end

function path = fix_slash(path0)
path = path0;
if ispc
    path(path=='\')='/';
end
end
