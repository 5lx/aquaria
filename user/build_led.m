%% LED
LED = legacy_code('initialize');
LED.SFunctionName = 'stellaris_lp_sfunc_LED'; % sfunction name
LED.HeaderFiles = {'led.h'}; % list your c code header files
LED.SourceFiles = {'led.c'}; % list  of your c code src files
LED.OutputFcnSpec = 'led_output(uint8 u1, uint8 u2, uint8 u3)';
LED.SampleTime = 'parameterized';
LED.IncPaths = {pwd};   % path of your c code header file 
LED.SrcPaths = {pwd};   % path of your c code src file
LED.Options.singleCPPMexFile = true; 
LED.Options.useTlcWithAccel = false;

realtime.internal.runStellarisLPCmd('buildblock',LED(:))
