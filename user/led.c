/*
 * led.c
 *
 *  Created on: Jan 15, 2014
 *      Author: Sippey
 */


#ifdef MATLAB_MEX_FILE
// build mex file

    // no code in this one.

#else
// build realtime target
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/gpio.h"
#include <stellaris_runtime.h>
#include "led.h"

void led_output(int r, int g, int b)
{
    // call stellaris firmware function to do the job
    MAP_GPIOPinWrite(GPIO_PORTF_BASE,  LED_ALL, 
            ((r!=0)?2:0) |((g!=0)?8:0) |((b!=0)?4:0) );
}



#endif
