function install_sippey_rtt(varargin)
%%
% ex= hwconnectinstaller. Setup. get( ) ; 
hwconnectinstaller.show(  );
hSetup = hwconnectinstaller.Setup.get(  );
ex = hSetup.Explorer;
%ex = hwconnectinstaller.show();
ex.hide;

root=ex.getRoot;
%setup=get(root,'MCOSObjectReference');
setup = root.getMCOSObjectReference;

mirrors = {'http://people.clemson.edu/~pxu/rtt_repo', ...  1
           'http://web-service.sippey.org/matlab_repo', ... 2
           'http://web-service.sippey.org/matlab_repo/debug', ... 3
           'https://bitbucket.org/sippey/aquaria/wiki/matlab_repo'};% 4
if nargin == 0
    selected_mirror = mirrors{1};
else
    selected_mirror = mirrors{str2double(varargin{1})};
end

setup.Installer.XmlHttp=selected_mirror;
%setup.CurrentStep.next('');
setup.Steps.Children(1).Children(3)=[]; % license page
setup.Steps.Children(1).Children(2)=[]; % login page
setup.CurrentStep.next('');
ex.title=[ex.title ' from Sippey''s repo'];
setup.Steps.Children(1).StepData.Labels.Internet='From Sippey''s Online Repo (Recommended)';
ex.show;
%ex.showTreeView(true);
end