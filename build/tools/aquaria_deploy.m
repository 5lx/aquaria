function aquaria_deploy(varargin)
   
    % determine parameters
    % aquaria_deploy [*release|debug] [cu|sorg|*sorgd]
    mode = 'release';
    repo = 'sorgd';
    if nargin > 1
        repo = varargin{2};
    end
    if nargin > 0 
        mode = varargin{1};
    end
    
    repo_url = i_getRepoUrl(repo);
        
    % find the code path
    [folder, ~, ~] = fileparts(which('aquaria_deploy'));
    aquaria_dir = fileparts(fileparts(folder));
    package_name = 'StellarisLP';
    
    build_dir = fullfile(aquaria_dir,'build','tmp',package_name);
    dev_dir  =  fullfile(aquaria_dir,'package');
    
    % switches
    pcode_mfile = strcmp(mode,'release');
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % preprocessing
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % build mex files
    current_dir = pwd();
    cd(fullfile(dev_dir,'blocks','autobuild'));
    %lctgen_cgir();
    cd(current_dir);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % copy files to build\tmp\[package_name]
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if exist(build_dir,'dir')
        rmdir(build_dir, 's');
    end
    mkdir(build_dir);
    
    % +realtime
    
    realtime_dir = fullfile(build_dir, '+realtime', '+internal');
    mkdir(realtime_dir);
    copyfile(fullfile(dev_dir,'+realtime','+internal', '*.m'), realtime_dir);
    if pcode_mfile
        cd(build_dir)
        pcode(realtime_dir);
        cd(current_dir);
        delete(fullfile(realtime_dir, '*.m'));
    end
    
    % blocks
    blocks_dir = fullfile(build_dir, 'blocks');
    mkdir(blocks_dir);
    copyfile(fullfile(dev_dir,'blocks','mex'), fullfile(blocks_dir,'mex'));
    copyfile(fullfile(dev_dir,'blocks','stellaris_lp_lct'), fullfile(blocks_dir,'stellaris_lp_lct'));
    copyfile(fullfile(dev_dir,'blocks','slblocks.m'), blocks_dir);
    copyfile(fullfile(dev_dir,'blocks','stellaris_lp_lib.mdl'), blocks_dir);
    
    % demo
    demo_dir = fullfile(build_dir, 'demo');
    mkdir(demo_dir);
    copyfile(fullfile(dev_dir,'demo','*.mdl'), demo_dir);
    copyfile(fullfile(dev_dir,'demo','*.slx'), demo_dir);    
    copyfile(fullfile(dev_dir,'demo','*.m'), demo_dir);
    copyfile(fullfile(dev_dir,'demo','*.fig'), demo_dir);
    copyfile(fullfile(dev_dir,'demo','demos.xml'), demo_dir);
    copyfile(fullfile(dev_dir,'demo','html'), fullfile(demo_dir,'html'));
    
    % registry and patch registry
    copyfile(fullfile(dev_dir,'registry'), fullfile(build_dir,'registry'));

    i_patch_repo_url(fullfile(build_dir,'registry','support_package_registry.xml'), repo_url);
    i_patch_repo_url(fullfile(build_dir,'registry','thirdparty_package_registry.xml'), repo_url);
    
    % rtw
    copyfile(fullfile(dev_dir,'rtw'), fullfile(build_dir, 'rtw'));
    
    % utils
    copyfile(fullfile(dev_dir,'utils'), fullfile(build_dir, 'utils'));    
    
    % src
    src_dir = fullfile(build_dir, 'src');
    mkdir(src_dir);
    copyfile(fullfile(dev_dir,'src','*.asm'), src_dir);
    copyfile(fullfile(dev_dir,'src','*.c'), src_dir);
    copyfile(fullfile(dev_dir,'src','*.cpp'), src_dir);
    copyfile(fullfile(dev_dir,'src','*.h'), src_dir);
    
    % root dir
    copyfile(fullfile(dev_dir,'sl_customization.m'), build_dir);
    copyfile(fullfile(dev_dir,'stellaris_lp_setup.m'), build_dir);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % packaging
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    maker = hwconnectinstaller.PackageMaker;
    maker.Configuration = mode;
    maker.RootDir = fileparts(build_dir);
    
    current_dir = pwd();
    cd(fullfile(aquaria_dir,'build','distr')); % distribute dir
    [ archivefile, xmlfile ]= maker.make(package_name, mode,'true');
    
    % get rid of the forced license file
    system(['zip -q -d "' archivefile '" "' package_name '/license.txt"']);
    rmdir(build_dir,'s');
    
    
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % copy files to build\tmp\[package_name]
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    third_package_name = 'StellarisLPThirdParty';
    third_dev_dir = fullfile(aquaria_dir,'build', third_package_name);
    build_dir = fullfile(aquaria_dir,'build','tmp', third_package_name);
    
    if exist(build_dir,'dir')
        rmdir(build_dir, 's');
    end
    mkdir(build_dir);
    
    % registry  and patch registry
    copyfile(fullfile(third_dev_dir,'registry'), fullfile(build_dir,'registry'));
    
    i_patch_repo_url(fullfile(build_dir,'registry','support_package_registry.xml'), repo_url);
    i_patch_repo_url(fullfile(build_dir,'registry','thirdparty_package_registry.xml'), repo_url);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % packaging support software dummy package
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [ archivefile2, xmlfile ]= maker.make(third_package_name,'release','true');
    
    % get rid of the forced license file
    system(['zip -q -d "' archivefile2 '" "' third_package_name '/license.txt"']);

    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % preparing repo registry file
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if exist('package_registry.xml','file')
        delete('package_registry.xml');
    end
    
    reg_files = dir('*_registry.xml');
    docNode = com.mathworks.xml.XMLUtils.createDocument('PackageRepository');
    for f = 1:length(reg_files)
        merge_registry_xml(docNode.getDocumentElement, reg_files(f).name);
        if strcmp(mode, 'release')
            delete(reg_files(f).name);
        end
    end
    xmlwrite('package_registry.xml', docNode);
    docNode=[]; %#ok<NASGU>
    
    %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % upload
    % http://people.clemson.edu/~pxu/rtt_repo
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(repo, 'cu')
        disp('Uploading to FTP ...');
        fc = clemson_ftp();
        mput(fc, archivefile);
        mput(fc, archivefile2);
        mput(fc,'package_registry.xml');
    else
        disp('Uploading using rsync ...');
        rsync_distr(repo);
    end
    
    
    %%
    % reset path
    cd(current_dir);
    disp('Done!');
end

function url = i_getRepoUrl(name)
switch name
    case 'sorgd'
        url = 'http://web-service.sippey.org/matlab_repo/debug';
    case 'sorg'
        url = 'http://web-service.sippey.org/matlab_repo';
    case 'cu'
        url = 'http://people.clemson.edu/~pxu/rtt_repo';
    case 'bbkt'
        url = 'https://bitbucket.org/sippey/aquaria/wiki/matlab_repo';
end
end

function i_patch_repo_url(xml_file_name, repo_url)
content = fileread(xml_file_name);
f = fopen(xml_file_name, 'w');
fwrite(f, strrep(content, '###REPO_URL###', repo_url));
fclose(f);
end