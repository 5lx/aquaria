function aquaria_swap(folder)
clear functions
%clear classes

package_name = 'StellarisLP';
install = hwconnectinstaller.PackageInstaller;
install_dir = install.getDefaultInstallDir;

folder = lower(folder);
package_dir = fullfile(install_dir, package_name);
package_dev_dir = fullfile(install_dir, [ package_name '_dev']);
package_test_dir = fullfile(install_dir, [package_name '_test']);

rmpath(package_dir, fullfile(package_dir,'blocks'), fullfile(package_dir,'blocks','mex'), ...
       fullfile(package_dir,'demo'),fullfile(package_dir,'utils'));
rehash path
rehash toolbox


if exist(package_dir, 'dir')
    if exist(fullfile(package_dir, 'dev_folder_id'), 'file')
        % development folder in its place
        if exist(package_dev_dir, 'dir')
            error('Duplicated development folder in install location');
        else
            i_rename_dir(install_dir, package_name, [ package_name '_dev']);
        end
    else
        % test folder in its place
        if exist(package_test_dir, 'dir')
            error('Duplicated test folder in install location');
        else
            i_rename_dir(install_dir, package_name, [ package_name '_test']);
        end
    end
end

switch folder
    case 'dev',
        i_rename_dir(install_dir, [ package_name '_dev'], package_name);
    case 'test',
        i_rename_dir(install_dir, [ package_name '_test'], package_name);
    otherwise,
        error('The folder should be one of DEV or TEST.');
end

addpath(package_dir, fullfile(package_dir,'blocks'), fullfile(package_dir,'blocks','mex'), ...
        fullfile(package_dir,'demo'),fullfile(package_dir,'utils'));
rehash path
rehash toolbox

end

function i_rename_dir(dir, current_name, new_name)
if ispc
    cmd = 'rename';
    system(['rename ' fullfile(dir, current_name) ' ' new_name]);
elseif isunix
    cmd = 'mv';
    system(['rename ' fullfile(dir, current_name) ' ' fullfile(dir, new_name)]);
end

end