function merge_xml( dom_obj, xml_file )
%MERGE_XML Summary of this function goes here
%   Detailed explanation goes here

dom_to_merge = xmlread(xml_file);

releases = dom_to_merge.getElementsByTagName('MatlabRelease');

for i=0:releases.getLength-1,
    rel = releases.item(i);
    
    release = getRelease(dom_obj, rel.getAttribute('name'));
    
    pkgs = rel.getElementsByTagName('SupportPackage');
    
    for j=0:pkgs.getLength-1,
        addPackage(release, pkgs.item(j));
    end
    
end

%output = dom_obj;
end

function addPackage(release, package)
pkgs = release.getElementsByTagName('SupportPackage');
name = package.getAttribute('name');

for i=0:pkgs.getLength-1,
    if strcmp(name, pkgs.item(i).getAttribute('name'))
        release.removeChild( pkgs.item(i));
    end
end

release.appendChild(release.getOwnerDocument.importNode(package,1));

end

function release = getRelease(dom_obj, name)
releases = dom_obj.getElementsByTagName('MatlabRelease');
release = [];

for i=0:releases.getLength-1,
    if strcmp(name, releases.item(i).getAttribute('name'))
        release = releases.item(i);
        break;
    end
end

if isempty(release)
    node = dom_obj.getOwnerDocument.createElement('MatlabRelease');
    node.setAttribute('name', name);
    dom_obj.appendChild(node);
    release = getRelease(dom_obj, name);
end

end
