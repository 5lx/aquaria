﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace LightRemoterDemo
{
    public struct DataPacket
    {
        public short a;
        public byte light;
        public byte c;
    }

    public partial class Form1 : Form
    {
        TcpClient tcpClientB;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataPacket pack;
            pack.a = short.Parse(textBox1.Text);
            pack.light = (byte)((checkBox1.Checked ? 2 : 0) + (checkBox2.Checked ? 4 : 0) + (checkBox3.Checked ? 8 : 0));
            pack.c = byte.Parse(textBox2.Text); ;
            // Translate the passed message into ASCII and store it as a Byte array.
            Byte[] data = getBytes(pack);
            try
            {
                NetworkStream stream = tcpClientB.GetStream();

                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, data.Length);
            }
            catch (SocketException ex)
            {
                Console.WriteLine("SocketException: {0}", ex);
            }
        }

        byte[] getBytes(DataPacket pack)
        {
            int size = Marshal.SizeOf(pack);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(pack, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try{ 
                tcpClientB= new TcpClient("127.0.0.1", 12202);
            }
            catch (SocketException ex) 
            {
                Console.WriteLine("SocketException: {0}", ex);
            }
        }
        private void Form1_Unload(object sender, EventArgs e)
        {
            tcpClientB.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }
        
    }
}
