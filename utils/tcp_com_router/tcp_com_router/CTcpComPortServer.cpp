#include "stdafx.h"

#include <process.h>
#include "CTcpComPortServer.h"
#include "CComHubServer.h"

CTcpComPortServer::CTcpComPortServer(int port, CComHubServer* comHub, bool autoReset):
						CTcpServer(12200+port),	m_port(port), CComPortEventHandler(port,comHub),
						m_activeSock(INVALID_SOCKET), m_autoReset(autoReset)
{

}

CTcpComPortServer::~CTcpComPortServer()
{

}


void CTcpComPortServer::TcpComPortServerTxThread(void *p)
{
	int len;
	char buffer;
	PTCPCOMPORT_SESSION p_sess=(PTCPCOMPORT_SESSION)p;
	while(!(p_sess->p->m_terminate))
	{
		len = recv(p_sess->sock_in, &buffer, 1, 0);
		if (!len || len == SOCKET_ERROR)
		{
			//socket error
			break;
		}
		p_sess->p->SendToHubCom(buffer);
	}
	p_sess->p->UnregisterToHubCom();
	closesocket(p_sess->sock_in);
	p_sess->p->m_activeSock = INVALID_SOCKET;
	delete p_sess;
}

void CTcpComPortServer::OnConnected(SOCKET sock_in, sockaddr_in* si_in)
{
	//m_sess = sock_in;

	printf("Incoming connect to port %d\n from local port %d\n", m_port, si_in->sin_port);

	if (m_activeSock != INVALID_SOCKET)
	{
		// if connected already, close incoming connection
		closesocket(sock_in);
	}
	PTCPCOMPORT_SESSION p_sess= new TCPCOMPORT_SESSION;
	p_sess->p = this;
	p_sess->sock_in = sock_in;
	m_activeSock = sock_in;

	RegisterToHubCom();
	_beginthread(TcpComPortServerTxThread, 0, p_sess);
}


void CTcpComPortServer::OnComRecv(char buffer)
{
	int len;
	if (m_activeSock != INVALID_SOCKET)
	{
		len = send(m_activeSock, &buffer, 1, 0);
	}
}

void CTcpComPortServer::OnResetConnection()
{
	if (m_activeSock != INVALID_SOCKET && m_autoReset)
	{
		closesocket(m_activeSock);
	}
}