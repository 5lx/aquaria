#pragma once


//#include <WinSock2.h>
#include "xPCUDPSock.h"

#include "CComPortEventHandler.h"

// com virtual port <= local udp port 1230x (CUDPReceiver)
//                  => local udp port 1240x (CUDPSender)

class CComHubServer;
class CUdpComPortServer: public CComPortEventHandler
{
public:
	CUdpComPortServer(int port, int size, CComHubServer* comHub);
	virtual ~CUdpComPortServer();

	// specification of CComPortEventHandler interface
	virtual void OnComRecv(char buffer);
	virtual void OnResetConnection();

private:
	static void UdpComPortServerTxThread(void *p);

	int				m_size;

	CUDPReceiver	m_recver;
	CUDPSender		m_sender;

	char*			m_recv_buf;
	int				m_recv_nbuf;

	char*			m_send_buf;
	int				m_send_nbuf;

	int				m_tx_running;
};