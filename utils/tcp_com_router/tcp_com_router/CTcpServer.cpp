
#include "stdafx.h"
#include <process.h>

#include "CTcpServer.h"

#pragma comment( lib, "ws2_32" )

bool CTcpServer::ConfigServer(unsigned short port, unsigned long ipaddr )
{
	if (m_terminate) // only config when it is stopped.
	{
		memset((char *) &m_si, 0, sizeof(m_si));
		m_si.sin_family = AF_INET;
		m_si.sin_port = htons(port);
		m_si.sin_addr.s_addr = ipaddr;
		return true;
	}else
		return false;
}

int CTcpServer::StartServer()
{
	m_sock = socket(AF_INET,SOCK_STREAM, IPPROTO_TCP);
	
	if (m_sock<0)
		return SOCKET_ERROR;

	m_terminate = false;

	if( bind(m_sock,(sockaddr*)&m_si,sizeof(sockaddr)) == SOCKET_ERROR ||
		(m_tid = _beginthread(ServerThread,0,this)) == -1 )
	{
		closesocket(m_sock);
		m_tid  = -1;
		m_sock = -1;
		m_terminate = true;
		return SOCKET_ERROR;
	}else
	{
		return 1;
	}
}

void CTcpServer::StopServer()
{
	if (m_tid != -1)
	{
		m_terminate = true;
		m_tid = -1;
	}
	if (m_sock!=-1)
	{
		closesocket(m_sock);
		m_sock = -1;
	}
}

void CTcpServer::OnConnected(SOCKET sock_in, sockaddr_in* si_in)
{
	// do nothing but close it.
	closesocket(sock_in);
}
void CTcpServer::ServerThread(void* p)
{
	CTcpServer *psvr= (CTcpServer *)p;

	SOCKET sock_in;
	sockaddr_in si_in;
	int len_sockaddr_in = sizeof(si_in);

	// Listen for an incomming connections...
	listen(psvr->m_sock,SOMAXCONN );

	while (!(psvr->m_terminate))
	{
		sock_in = accept(psvr->m_sock,(sockaddr*)&si_in, &len_sockaddr_in);
		if (sock_in!=INVALID_SOCKET )
		{
			psvr->OnConnected(sock_in, &si_in);
		}else
		{
			int err;
			err = ::WSAGetLastError();
		}
	}
}

CTcpServer::CTcpServer(unsigned short port, unsigned long ipaddr)
	: m_tid(-1),m_sock(-1),m_terminate(true)
{
	ConfigServer(port, ipaddr);
}

CTcpServer::CTcpServer(unsigned short port, char * ipaddr_str)
	: m_tid(-1),m_sock(-1),m_terminate(true)
{
	ConfigServer(port, inet_addr(ipaddr_str));
}

CTcpServer::CTcpServer(unsigned short port)
	: m_tid(-1),m_sock(-1),m_terminate(true)
{
	ConfigServer(port, INADDR_ANY);
}
CTcpServer::~CTcpServer()
{
	StopServer();
}
