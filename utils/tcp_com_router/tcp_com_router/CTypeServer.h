#pragma once

#include "CTcpServer.h"

class CTypingServer: public CTcpServer
{
protected:
	virtual void OnConnected(SOCKET sock_in, sockaddr_in* si_in);
private:
	typedef struct tagTYPING_SESSION
	{
		CTypingServer* p;
		SOCKET sock;
	}TYPING_SESSION, *PTYPING_SESSION;
	static void TypeServerThread(void *p);
};