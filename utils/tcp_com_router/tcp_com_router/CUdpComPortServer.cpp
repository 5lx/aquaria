#include "stdafx.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <process.h>
#include <WinSock2.h>

#include "CUdpComPortServer.h"
#include "CComHubServer.h"

CUdpComPortServer::CUdpComPortServer(int port, int size, CComHubServer* comHub): CComPortEventHandler(port,comHub),
		m_recver(size, 12300+port), m_sender(size, 12400+port, "127.0.0.1"),m_size(size)
{
	m_recv_buf = new char[size];
	m_send_buf=  new char[size];

	m_recv_nbuf = m_send_nbuf=0;

	RegisterToHubCom();

	m_tx_running = 1;
	_beginthread(UdpComPortServerTxThread, 0, this);
}

CUdpComPortServer::~CUdpComPortServer()
{
	m_tx_running = 0;

	if (m_recv_buf)
		delete [] m_recv_buf;
	if (m_send_buf)
		delete [] m_send_buf;
}

void CUdpComPortServer::OnComRecv(char buffer)
{
	m_send_buf[m_send_nbuf++] = buffer;
	if (m_send_nbuf==m_size)
	{
		m_sender.SendData(m_send_buf);
		m_send_nbuf=0;
	}
}

void CUdpComPortServer::OnResetConnection()
{
	// not connection based, so do nothing
}

void CUdpComPortServer::UdpComPortServerTxThread(void *p)
{
	CUdpComPortServer* svr = (CUdpComPortServer* )p;
	int i;

	while(svr->m_tx_running)
	{
		Sleep(1);
		svr->m_recver.GetData(svr->m_recv_buf);

		if (!svr->m_tx_running) break;
		for (i=0; i<svr->m_size;++i)
		{
			// send package sep;
			svr->SendToHubCom(svr->m_recv_buf[i]);
		}
	}
}