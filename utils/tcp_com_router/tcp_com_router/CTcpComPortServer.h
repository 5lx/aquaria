#pragma once

#include "CTcpServer.h"
#include "CComPortEventHandler.h"

class CComHubServer;
class CTcpComPortServer: public CTcpServer, public CComPortEventHandler 
{
public:
	CTcpComPortServer(int port, CComHubServer* comHub, bool auto_reset=false);
	virtual ~CTcpComPortServer();

	// specification of CComPortEventHandler interface
	virtual void OnComRecv(char buffer);
	virtual void OnResetConnection();

protected:
	virtual void OnConnected(SOCKET sock_in, sockaddr_in* si_in);
private:
	typedef struct tagTCPCOMPORT_SESSION
	{
		CTcpComPortServer* p;
		SOCKET sock_in;
	}TCPCOMPORT_SESSION, *PTCPCOMPORT_SESSION;
	
	static void TcpComPortServerTxThread(void *p);
	
	SOCKET			m_activeSock;
	int				m_port;
	bool			m_autoReset;
};