#pragma once

class CComHubServer;
class CComPortEventHandler
{
public:
	CComPortEventHandler(int port, CComHubServer* comHub);
	virtual ~CComPortEventHandler(){}
	virtual void OnComRecv(char buffer)=0;
	virtual void OnResetConnection()=0;

protected:
	void RegisterToHubCom();
	void UnregisterToHubCom();
	void SendToHubCom(char buffer);
private:
	CComHubServer * m_comHub;
	int				m_port;
};

