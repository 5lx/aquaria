// tcp_com_router.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CComHubServer.h"
#include "CTcpComPortServer.h"
#include "CUdpComPortServer.h"
#include "IniReader.h"

#define MAX_N_PORTS 10
#define SETTING_FILENAME "setting.ini"

using namespace std;

void test_run();

void init_socket()
{
	WSADATA wi;
	::WSAStartup(0x0202,&wi);
}

int _tmain(int argc, _TCHAR* argv[])
{

	CComHubServer *pComServer;
	CComPortEventHandler *pPortHandler[MAX_N_PORTS];

	init_socket();
	//test_run();
	//getchar();

	pComServer=NULL;
	memset(pPortHandler,0,sizeof(pPortHandler[0])*MAX_N_PORTS);

	INIReader ini(SETTING_FILENAME);

	try	{
		int port;
		int baud;
		port  = ini.GetInteger("SERIAL","PORT", -1);
		baud  = ini.GetInteger("SERIAL","BAUD", 0);

		if (port == -1)
		{
			throw("ERROR: Missing or incorrect serial settings.");
		}

		printf("Open serial COM%d at %d bps\n", port, baud);
		pComServer = new CComHubServer(port, baud);
	}catch (const char* e) {
		printf("%s\n\n", e);
		return 1;
	}

	try	{
		char sectionName[10];
		for(int i=0;i<MAX_N_PORTS;++i)
		{
			sprintf(sectionName, "PORT%d",i); 
			string type = ini.Get( sectionName, "TYPE","");
			if (type.compare( "TCP") ==0)
			{
				int autoReset = ini.GetInteger(sectionName,"AUTORESET", 0);
				CTcpComPortServer *svr;

				printf("Open port %d in TCP mode (AutoReset=%d)\n", i, autoReset);
				svr = new CTcpComPortServer(i, pComServer, autoReset!=0);
				svr->StartServer();
				pPortHandler[i]=svr;
			}else if (type.compare( "UDP") ==0)
			{
				int size = ini.GetInteger(sectionName,"SIZE", 0);

				if (size !=0)
				{
					printf("Open port %d in UDP mode (size=%d)\n", i, size);
					pPortHandler[i] = new CUdpComPortServer(i, size, pComServer);
				}
			}
		}
	}catch (const char* e) {
		printf("%s\n\n", e);
		return 1;
	}

	printf("Intialization Finished\n\n");
	while(1)
	{
		Sleep(1000);
	}
	//test_run();
	return 0;
}

void test_run()
{
	int res;

	CComHubServer comhub(6, 115200);
	CTcpComPortServer port0(0, &comhub,true);
	CTcpComPortServer port1(1, &comhub);
	CTcpComPortServer port2(2, &comhub);
	CUdpComPortServer port3(3, 6, &comhub);

	res = port0.StartServer();
	printf("Open port 0 server ... %s\n", (res == SOCKET_ERROR)?"Failed!":"Succeed!");

	res = port1.StartServer();
	printf("Open port 1 server ... %s\n", (res == SOCKET_ERROR)?"Failed!":"Succeed!");

	res = port2.StartServer();
	printf("Open port 2 server ... %s\n", (res == SOCKET_ERROR)?"Failed!":"Succeed!");

	printf("Open port 3 server (UDP) ... Succeed!\n");

	getchar();

}

