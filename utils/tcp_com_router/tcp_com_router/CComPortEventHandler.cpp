#include "stdafx.h"

#include "CComPortEventHandler.h"
#include "CComHubServer.h"

CComPortEventHandler::CComPortEventHandler(int port, CComHubServer* comHub): m_comHub(comHub), m_port(port)
{
}

void CComPortEventHandler::RegisterToHubCom()
{
	m_comHub->RegisterTcp(m_port, this);
}

void CComPortEventHandler::UnregisterToHubCom()
{
	m_comHub->UnregisterTcp(m_port);
}

void CComPortEventHandler::SendToHubCom(char buffer)
{
	m_comHub->Send(m_port, buffer);
}